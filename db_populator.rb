require './config/environment'

require "json"

# states_file = File.read("states_india_clean.geojson")
# data = JSON.parse(states_file)
#
# features = data['features']
# features.each do |feature|
#   state = State.new
#   state.state_code = feature['properties']['ST_CODE']
#   state.state_name = feature['properties']['ST_NAME']
#   state.total_pcs = 0
#   state.geometry_type = feature['geometry']['type']
#   state.geometry_coordinates = feature['geometry']['coordinates']
#   state.save
#   puts ('#{state.state_code} saved')
# end

# states_file = File.read("pcs_india_clean.geojson")
# data = JSON.parse(states_file)
#
# features = data['features']
# features.each do |feature|
#   pc = ParliamentaryConstituency.new
#   st_code = feature['properties']['ST_CODE']
#   st_name = feature['properties']['ST_NAME']
#   pc.pc_code = feature['properties']['PC_CODE']
#   pc.pc_name = feature['properties']['PC_NAME']
#   pc.geometry_type = feature['geometry']['type']
#   pc.geometry_coordinates = feature['geometry']['coordinates']
#   states = State.where(["state_code = :u", { u: st_code }])
#   pc.state = states.first.id
#   pc.save
# end

State.all.each do |state|
  pcs = ParliamentaryConstituency.where(["state = :u", { u: state.id }])
  if(state.state_code == 'JK')
    state.total_pcs = pcs.length - 1
  else
    state.total_pcs = pcs.length
  end
  state.save
end