class CreateParliamentaryConstituencies < ActiveRecord::Migration[5.2]
  def change
    create_table :parliamentary_constituencies do |t|
      t.string :pc_code
      t.string :pc_name
      t.string :geometry_type
      t.text :geometry_coordinates

      t.timestamps
    end
  end
end
