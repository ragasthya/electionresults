class CreateCandidates < ActiveRecord::Migration[5.2]
  def change
    create_table :candidates do |t|
      t.integer :serial_number
      t.string :candidate_name
      t.integer :total_votes
      t.string :photograph
      t.integer :political_party
      t.integer :constituency

      t.timestamps
    end
  end
end
