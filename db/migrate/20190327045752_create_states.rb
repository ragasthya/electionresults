class CreateStates < ActiveRecord::Migration[5.2]
  def change
    create_table :states do |t|
      t.string :state_code
      t.string :state_name
      t.integer :total_pcs
      t.string :geometry_type
      t.text :geometry_coordinates

      t.timestamps
    end
  end
end
