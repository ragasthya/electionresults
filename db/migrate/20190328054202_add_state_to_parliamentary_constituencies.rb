class AddStateToParliamentaryConstituencies < ActiveRecord::Migration[5.2]
  def change
    add_column :parliamentary_constituencies, :state, :integer
  end
end
