class CreatePoliticalParties < ActiveRecord::Migration[5.2]
  def change
    create_table :political_parties do |t|
      t.string :party_code
      t.string :party_name
      t.string :party_color
      t.string :party_symbol

      t.timestamps
    end
  end
end
