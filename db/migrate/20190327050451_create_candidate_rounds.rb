class CreateCandidateRounds < ActiveRecord::Migration[5.2]
  def change
    create_table :candidate_rounds do |t|
      t.integer :total_votes
      t.integer :candidate
      t.integer :round_number

      t.timestamps
    end
  end
end
