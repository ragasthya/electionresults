class CreateRounds < ActiveRecord::Migration[5.2]
  def change
    create_table :rounds do |t|
      t.integer :round_number
      t.integer :total_votes
      t.integer :constituency

      t.timestamps
    end
  end
end
