json.extract! round, :id, :round_number, :total_votes, :constituency, :created_at, :updated_at
json.url round_url(round, format: :json)
