json.extract! candidate_round, :id, :total_votes, :candidate, :round_number, :created_at, :updated_at
json.url candidate_round_url(candidate_round, format: :json)
