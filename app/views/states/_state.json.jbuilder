json.extract! state, :id, :state_code, :state_name, :total_pcs, :geometry_type, :geometry_coordinates, :created_at, :updated_at
json.url state_url(state, format: :json)
