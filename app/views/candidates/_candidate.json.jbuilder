json.extract! candidate, :id, :serial_number, :candidate_name, :total_votes, :photograph, :political_party, :constituency, :created_at, :updated_at
json.url candidate_url(candidate, format: :json)
