json.extract! political_party, :id, :party_code, :party_name, :party_color, :party_symbol, :created_at, :updated_at
json.url political_party_url(political_party, format: :json)
