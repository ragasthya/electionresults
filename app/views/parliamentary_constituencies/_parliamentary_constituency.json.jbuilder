json.extract! parliamentary_constituency, :id, :pc_code, :pc_name, :geometry_type, :geometry_coordinates, :created_at, :updated_at
json.url parliamentary_constituency_url(parliamentary_constituency, format: :json)
