class ParliamentaryConstituenciesController < ApplicationController
  before_action :set_parliamentary_constituency, only: [:show, :edit, :update, :destroy]

  # GET /parliamentary_constituencies
  # GET /parliamentary_constituencies.json
  def index
    @parliamentary_constituencies = ParliamentaryConstituency.all
  end

  # GET /parliamentary_constituencies/1
  # GET /parliamentary_constituencies/1.json
  def show
  end

  # GET /parliamentary_constituencies/new
  def new
    @parliamentary_constituency = ParliamentaryConstituency.new
  end

  # GET /parliamentary_constituencies/1/edit
  def edit
  end

  # POST /parliamentary_constituencies
  # POST /parliamentary_constituencies.json
  def create
    @parliamentary_constituency = ParliamentaryConstituency.new(parliamentary_constituency_params)

    respond_to do |format|
      if @parliamentary_constituency.save
        format.html { redirect_to @parliamentary_constituency, notice: 'Parliamentary constituency was successfully created.' }
        format.json { render :show, status: :created, location: @parliamentary_constituency }
      else
        format.html { render :new }
        format.json { render json: @parliamentary_constituency.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /parliamentary_constituencies/1
  # PATCH/PUT /parliamentary_constituencies/1.json
  def update
    respond_to do |format|
      if @parliamentary_constituency.update(parliamentary_constituency_params)
        format.html { redirect_to @parliamentary_constituency, notice: 'Parliamentary constituency was successfully updated.' }
        format.json { render :show, status: :ok, location: @parliamentary_constituency }
      else
        format.html { render :edit }
        format.json { render json: @parliamentary_constituency.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /parliamentary_constituencies/1
  # DELETE /parliamentary_constituencies/1.json
  def destroy
    @parliamentary_constituency.destroy
    respond_to do |format|
      format.html { redirect_to parliamentary_constituencies_url, notice: 'Parliamentary constituency was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_parliamentary_constituency
      @parliamentary_constituency = ParliamentaryConstituency.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def parliamentary_constituency_params
      params.require(:parliamentary_constituency).permit(:pc_code, :pc_name, :geometry_type, :geometry_coordinates)
    end
end
