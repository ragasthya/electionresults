class RoundsController < ApplicationController
  before_action :set_round, only: [:show, :edit, :update, :destroy]

  # GET /rounds
  # GET /rounds.json
  def index
    @rounds = Round.all
  end

  # GET /rounds/1
  # GET /rounds/1.json
  def show
  end

  # GET /rounds/new
  def new
    @constituency = ParliamentaryConstituency.find(params[:constituency])
    @rounds = Round.where(["constituency = :u", { u: @constituency.id }]).length + 1
    @round = Round.new
    @round.round_number = @rounds
    @round.constituency = @constituency.id
    @round.total_votes = 0
    @candidates = Candidate.where(["constituency = :u", { u: @constituency.id }])
  end

  # GET /rounds/1/edit
  def edit
  end

  # POST /rounds
  # POST /rounds.json
  def create
    @round = Round.new(round_params)
    respond_to do |format|
      if @round.save
        @candidates = Candidate.where(["constituency = :u", { u: round_params[:constituency] }])
        @candidates.each do |candidate|
          candidate_round = CandidateRound.new
          candidate_round.candidate = candidate.id
          candidate_round.round_number = @round.id
          candidate_round.total_votes = 0
          candidate_round.save
        end
        format.html { redirect_to @round, notice: 'Round was successfully created.' }
        format.json { render :show, status: :created, location: @round }
      else
        format.html { render :new }
        format.json { render json: @round.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /rounds/1
  # PATCH/PUT /rounds/1.json
  def update
    respond_to do |format|
      if @round.update(round_params)
        format.html { redirect_to @round, notice: 'Round was successfully updated.' }
        format.json { render :show, status: :ok, location: @round }
      else
        format.html { render :edit }
        format.json { render json: @round.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rounds/1
  # DELETE /rounds/1.json
  def destroy
    constituency = ParliamentaryConstituency.find(@round.constituency)
    candidate_rounds = CandidateRound.where(["round_number = :u", { u: @round.id }])
    candidate_rounds.each do |round|
      candidate = Candidate.find(round.candidate)
      candidate.total_votes -= round.total_votes
      if(candidate.total_votes < 0)
        candidate.total_votes = 0
      end
      candidate.save
      round.destroy
    end
    @round.destroy
    respond_to do |format|
      format.html { redirect_to constituency, notice: 'Round was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_round
      @round = Round.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def round_params
      params.require(:round).permit(:round_number, :total_votes, :constituency)
    end
end
