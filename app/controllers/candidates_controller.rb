class CandidatesController < ApplicationController
  before_action :set_candidate, only: [:show, :edit, :update, :destroy]

  # GET /candidates
  # GET /candidates.json
  def index
    @candidates = Candidate.all
  end

  # GET /candidates/1
  # GET /candidates/1.json
  def show
  end

  # GET /candidates/new
  def new
    @constituency = ParliamentaryConstituency.find(params[:constituency])
    @candidate = Candidate.new
    @candidate.constituency = @constituency.id
    @candidate.total_votes = 0
  end

  # GET /candidates/1/edit
  def edit
    @constituency = ParliamentaryConstituency.find(params[:constituency])
  end

  # POST /candidates
  # POST /candidates.json
  def create
    @candidate = Candidate.new(candidate_params)
    respond_to do |format|
      if @candidate.save
        constituency = ParliamentaryConstituency.find(@candidate.constituency)
        format.html { redirect_to constituency, notice: 'Candidate was successfully created.' }
        format.json { render :show, status: :created, location: @candidate }
      else
        format.html { render :new }
        format.json { render json: @candidate.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /candidates/1
  # PATCH/PUT /candidates/1.json
  def update
    constituency = ParliamentaryConstituency.find(@candidate.constituency)
    respond_to do |format|
      if @candidate.update(candidate_params)
        format.html { redirect_to constituency, notice: 'Candidate was successfully updated.' }
        format.json { render :show, status: :ok, location: @candidate }
      else
        format.html { render :edit }
        format.json { render json: @candidate.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /candidates/1
  # DELETE /candidates/1.json
  def destroy
    constituency = ParliamentaryConstituency.find(@candidate.constituency)
    rounds = CandidateRound.where(["candidate = :u", { u: @candidate.id }])
    rounds.each do |round|
      round.destroy
    end
    @candidate.destroy
    respond_to do |format|
      format.html { redirect_to constituency, notice: 'Candidate was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_candidate
      @candidate = Candidate.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def candidate_params
      params.require(:candidate).permit(:serial_number, :candidate_name, :total_votes, :photograph, :political_party, :constituency)
    end
end
