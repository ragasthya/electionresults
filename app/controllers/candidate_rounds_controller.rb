class CandidateRoundsController < ApplicationController
  before_action :set_candidate_round, only: [:show, :edit, :update, :destroy]

  # GET /candidate_rounds
  # GET /candidate_rounds.json
  def index
    @candidate_rounds = CandidateRound.all
  end

  # GET /candidate_rounds/1
  # GET /candidate_rounds/1.json
  def show
  end

  # GET /candidate_rounds/new
  def new
    @candidate_round = CandidateRound.new
  end

  # GET /candidate_rounds/1/edit
  def edit
  end

  # POST /candidate_rounds
  # POST /candidate_rounds.json
  def create
    @candidate_round = CandidateRound.new(candidate_round_params)

    respond_to do |format|
      if @candidate_round.save
        format.html { redirect_to @candidate_round, notice: 'Candidate round was successfully created.' }
        format.json { render :show, status: :created, location: @candidate_round }
      else
        format.html { render :new }
        format.json { render json: @candidate_round.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /candidate_rounds/1
  # PATCH/PUT /candidate_rounds/1.json
  def update
    respond_to do |format|
      if @candidate_round.update(candidate_round_params)
        total_votes = candidate_round_params[:total_votes]
        round = Round.where(["id = :u", { u: candidate_round_params[:round_number] }]).first
        round.total_votes = (round.total_votes.to_i + total_votes.to_i).to_s
        round.save
        candidate = Candidate.where(["id = :u", { u: candidate_round_params[:candidate] }]).first
        candidate.total_votes = (candidate.total_votes.to_i + total_votes.to_i).to_s
        candidate.save
        # constituency = ParliamentaryConstituency.where(["id = :u", { u: candidate.constituency }]).first
        format.html { redirect_to round, notice: 'Candidate round was successfully updated.' }
        format.json { render :show, status: :ok, location: @candidate_round }
      else
        format.html { render :edit }
        format.json { render json: @candidate_round.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /candidate_rounds/1
  # DELETE /candidate_rounds/1.json
  def destroy
    @candidate_round.destroy
    respond_to do |format|
      format.html { redirect_to candidate_rounds_url, notice: 'Candidate round was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_candidate_round
      @candidate_round = CandidateRound.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def candidate_round_params
      params.require(:candidate_round).permit(:total_votes, :candidate, :round_number)
    end
end
