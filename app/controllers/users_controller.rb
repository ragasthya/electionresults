class UsersController < ApplicationController
  def index
  end

  def approve_user
    user = User.find(params[:user_id])
    user.add_role "approved"
    user.save
    redirect_to :users_index, notice: "Successfully approved the user."
  end

  def destroy
    user = User.find(params[:user_id])
    user.destroy
    respond_to do |format|
      format.html { redirect_to users_index, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
end
