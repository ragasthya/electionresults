Rails.application.routes.draw do
  get 'users/index'
  get 'users/approve_user'
  get 'users/destroy'
  devise_for :users
  resources :candidate_rounds
  resources :rounds
  resources :candidates
  resources :political_parties
  resources :parliamentary_constituencies
  resources :states
  root to: "states#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
